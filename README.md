# Express with Docker

without Docker:

```
npm install
npm start
```

with Docker:

```
docker build -t express_example .
docker run -d -p 5010:5010 --rm express_example
```