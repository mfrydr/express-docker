###
## Example: The most basic, CORRECT, Dockerfile for Node.js
###

# use slim and the lastest debian distro offered
FROM node:20.11-bullseye-slim

EXPOSE 5010

# add user first, then set WORKDIR to set permissions
# Without this, Docker runs the process as root
USER node

WORKDIR /app

# copy in with correct permissions. 
# Using * prevents errors if file is missing
COPY --chown=node:node package*.json ./

# use ci to only install packages from lock files
RUN npm ci --omit=dev && npm cache clean --force

# copy files with correct permissions
COPY --chown=node:node . .

ENTRYPOINT ["node", "app.mjs"]

# Incorrect, won't forward signals
# CMD node index.js
# Better but should use ENTRYPOINT instead
# CMD ["node", "index.js"]