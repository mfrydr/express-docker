import express from 'express';

const app = express();

app.use('/', (req, res) => res.status(200).send('HEALTHY'));

const { SERVER_PORT: port = 5010 } = process.env;

const connection = app.listen({ port }, () => {
  console.log(`🚀 Server ready at ${port}`);
});

// Trap signals for graceful shutdown
const shutdown = () => {
  console.log('Stopping ...');
  connection.close(() => {
    console.log('Stopped');
  });
};
process.on('SIGINT', shutdown);
// docker stop sends SIGTERM
process.on('SIGTERM', shutdown);

// even better
/* eslint-disable no-unused-vars */
async function closeGracefully(signal) {
  console.log(`Received signal to terminate: ${signal}`);

  // await db.close() if we have a db connection in this app
  // await other things we should cleanup nicely
  process.kill(process.pid, signal);
}

// process.once('SIGINT', closeGracefully);
// process.once('SIGTERM', closeGracefully);
